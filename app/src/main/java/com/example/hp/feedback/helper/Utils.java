package com.example.hp.feedback.helper;

/**
 * Created by Dell on 4/21/2018.
 */
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by DELL on 4/17/2018.
 */

public class Utils {


    public static String TAG = "B=B Utils";

    public static final String PREFS_NAME = "SHARED_PREFERENCE_FEEDBACK";

    public static SharedPreferences getSharedPreferences(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_MULTI_PROCESS );
        return sharedPreferences;

    }
    public static void setSharedPreferences(Context context, String key, String value) {

        Log.d(TAG,"setSharedPreferences");
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.commit();
    }
    public static String getSharedPreferences(Context context,String key,String defaultValue) {

        Log.d(TAG,"getSharedPreferences");
        SharedPreferences sharedPreferences = getSharedPreferences(context);

        String value = defaultValue;

        if(key !=null)
        {
            value=  sharedPreferences.getString(key, defaultValue);
        }

        return value;
    }
}
