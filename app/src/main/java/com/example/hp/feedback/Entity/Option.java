package com.example.hp.feedback.Entity;

import java.util.List;

/**
 * Created by chintz on 11/10/17.
 */

public class Option {
    int OptionsID;
    String question;



    public Option()
    {

    }

    public Option(int OptionsID, String question) {
        this.OptionsID = OptionsID;
        this.question = question;

    }


    public int getOptionsID() {
        return OptionsID;
    }

    public void setOptionsID(int optionsID) {
        OptionsID = optionsID;
    }

    public String getOption() {
        return question;
    }

    public void setOption(String question) {
        question = question;
    }



}
