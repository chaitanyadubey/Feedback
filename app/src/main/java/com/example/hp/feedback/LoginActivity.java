package com.example.hp.feedback;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hp.feedback.helper.Utils;


public class LoginActivity extends AppCompatActivity {

    private static EditText editText_username;
    private static EditText editText_password;
    private static Button button_login;

    String TAG = "SAIT LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        RegistrationButton();

        Log.d(TAG,"onCreate");
        editText_username = (EditText)findViewById(R.id.editText_username);
        editText_password = (EditText)findViewById(R.id.editText_password);

        String userName = Utils.getSharedPreferences(this,"USER_NAME",null);

        if(userName!=null)
        {
            editText_username.setText(userName);

        }




    }
    public void RegistrationButton(){

        button_login = (Button)findViewById(R.id.button_login);

        button_login.setOnClickListener(
                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {

                        Toast.makeText(LoginActivity.this, "successful login", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent("com.example.hp.feedback.FacultyActivity");
                        startActivity(intent);

                        Utils.setSharedPreferences(LoginActivity.this,"USER_NAME",editText_username.getText().toString());


//                        if(username.getText().toString().equals("user") && password.getText().toString().equals("pass"))
//                        {
//                            Toast.makeText(LoginActivity.this, "successful login", Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent("com.example.hp.feedback.FacultyActivity");
//                            startActivity(intent);
//
//
//
//
//                        }else{
//                            Toast.makeText(LoginActivity.this, "Unsuccessful login", Toast.LENGTH_SHORT).show();
//                        }
                    }
                }
        );
    }
}
