package com.example.hp.feedback;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;

public class WelcomeActivity extends AppCompatActivity {

    LinearLayout l1, l2;
    Animation uptodown, downtoup;

    int SPLASH_DISPLAY_DURATION = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        //onClickButtonListener();
        l1=(LinearLayout)findViewById(R.id.l1);
        l2 = (LinearLayout)findViewById(R.id.l2);
        uptodown = AnimationUtils.loadAnimation(this, R.anim.uptodown);
        downtoup = AnimationUtils.loadAnimation(this, R.anim.downtoup);
        l1.setAnimation(uptodown);
        l2.setAnimation(downtoup);



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(WelcomeActivity.this, LoginActivity.class);
                WelcomeActivity.this.startActivity(mainIntent);
                WelcomeActivity.this.finish();
                finish();

            }
        }, SPLASH_DISPLAY_DURATION);


    }
//    public void onClickButtonListener(){
//        btn = (Button)findViewById(R.id.button);
//        btn.setOnClickListener(
//                new View.OnClickListener(){
//
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent("com.example.hp.feedback.LoginActivity");
//                        startActivity(intent);
//                    }
//                }
//        );
//    }

}
