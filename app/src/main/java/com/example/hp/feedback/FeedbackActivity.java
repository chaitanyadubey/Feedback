package com.example.hp.feedback;


import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.hp.feedback.Entity.Option;
import com.example.hp.feedback.Entity.Question;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class FeedbackActivity extends AppCompatActivity {

    String TAG="SAIT"+getClass();

    TextView textView_question;
    RadioButton rda, rdb, rdc, rdd;
    Button btnNext;

    ArrayList arrayListQuestions;
    JSONArray jsonarray_master;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);


        textView_question=(TextView)findViewById(R.id.textView_question);

        try {
            String jsonTxt = getTextFromAssestFile("questionnaire.json");

            //  Log.d(TAG, "jsonTxt=" + jsonTxt);

            jsonarray_master = new JSONArray(jsonTxt);

        } catch (JSONException e) {

            Log.d(TAG, "JSONException=" + e.getMessage());

            e.printStackTrace();
        }



        try {
            JSONArray jsonarray_entryQuiz = new JSONArray(jsonarray_master);
            Option option;
            ArrayList<Option> arrayListOption;

            Log.d(TAG, "jsonarray_entryQuiz=" + jsonarray_entryQuiz);
            Question  question;


            for (int i = 0; i < jsonarray_entryQuiz.length(); i++)

            {
                JSONObject jsonobject = jsonarray_entryQuiz.getJSONObject(i);

                question = new Question();



                String ques = jsonobject.getString("question");

                question.setQuestion(ques);


//                arrayListOption = new ArrayList<Option>();
//
//                JSONArray jsonArrayOptions = jsonobject.getJSONArray("Options");
//                for (int j = 0; j < jsonArrayOptions.length(); j++) {
//
//                    Log.d(TAG, "jsonArrayOptions" + jsonArrayOptions.toString());
//
//
//                    JSONObject jsonobjectOptions = jsonArrayOptions.getJSONObject(j);
//
//                    option = new Option();
//
//
//                    int OptionsID = jsonobjectOptions.getInt("OptionsID");
//                    option.setOptionsID(OptionsID);
//
//                    Name = jsonobjectOptions.getString("Name").toString();
//                    option.setName(Name);
//
//
//                    String Correct = jsonobjectOptions.getString("Correct");
//
//                    option.setCorrectString(Correct);
//
//                    arrayListOption.add(option);



//                question.setAnswerOptions(arrayListOption);

                Log.d(TAG, "Questions list " + i + "=" + question);

                arrayListQuestions.add(question);


            }


//            adapter.notifyDataSetChanged();
//            recyclerView.setAdapter(adapter);

            Log.d(TAG, "arrayListQuestions.count=" + arrayListQuestions.toString());


        } catch (JSONException e) {

            Log.d(TAG, "JSONException=" + e.getMessage());

            e.printStackTrace();
        }


//        loadNextQuestion();


    }


    public String getTextFromAssestFile(String jsonFileName) {
        String json = null;
        try {
            InputStream is = getAssets().open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}