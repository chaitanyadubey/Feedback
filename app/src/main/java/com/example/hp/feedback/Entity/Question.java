package com.example.hp.feedback.Entity;

import java.util.List;

/**
 * Created by chintz on 29/8/17.
 */

public class Question {

    String  question;
    List<Option> answerOptions  ;



    public Question()
    {

    }

    public Question(int QuestionID, String question, List<Option> answerOptions) {

        this.question = question;
        this.answerOptions = answerOptions;
    }



    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        question = question;
    }

    public List<Option> getAnswerOptions() {
        return answerOptions;
    }

    public void setAnswerOptions(List<Option> answerOptions) {
        this.answerOptions = answerOptions;
    }



    @Override
    public String toString() {

        return question+"\n"+answerOptions.toString();

    }




}
