package com.example.hp.feedback;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FacultyActivity extends AppCompatActivity {
   private static Button btn_1, btn_2, btn_3, btn_4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty);
        onClickButtonListener();
    }
    public void onClickButtonListener(){
        btn_1 = (Button)findViewById(R.id.btn1);
        btn_1.setOnClickListener(
                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent("com.example.hp.feedback.FeedbackActivity");
                        startActivity(intent);
                    }
                } );
        btn_2 = (Button)findViewById(R.id.btn2);
        btn_2.setOnClickListener(
                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent("com.example.hp.feedback.FeedbackActivity");
                        startActivity(intent);
                    }
                } );
        btn_3 = (Button)findViewById(R.id.btn3);
        btn_3.setOnClickListener(
                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent("com.example.hp.feedback.FeedbackActivity");
                        startActivity(intent);
                    }
                } );
        btn_4 = (Button)findViewById(R.id.btn4);
        btn_4.setOnClickListener(
                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent("com.example.hp.feedback.FeedbackActivity");
                        startActivity(intent);
                    }
                } );

}}